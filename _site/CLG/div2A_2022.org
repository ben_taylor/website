#+STARTUP: showeverything

#+title: Iománaíocht / Hurling -- 2022
#+subtitle: Division 2A
#+author: Benjamin Taylor, Kevin Hayes and OTHERS
#+date: Thu Aug 10 16:25:09 2023 

* Overall Team Strength


#+CAPTION: Team Strength
#+ATTR_HTML: :width 500px
[[./fig/strength20222A-1.png]]


* Goal and Point Scoring Strength


#+CAPTION: Goal Scoring Strength
#+ATTR_HTML: :width 500px
[[./fig/strengthg20222A-1.png]]
#+CAPTION: Point Scoring Strength
#+ATTR_HTML: :width 500px
[[./fig/strengthp20222A-1.png]]


* Home Pitch Advantage


#+CAPTION: Home Pitch Advantage
#+ATTR_HTML: :width 500px
[[./fig/homeadv20222A-1.png]]


* Model Diagnostic Plots


#+ATTR_HTML: :width 500px :center nil
#+CAPTION: Residuals (1)
[[./fig/residuals20222A-1.png]]
#+ATTR_HTML: :width 500px :center nil
#+CAPTION: Residuals (2)
[[./fig/fitres20222A-1.png]]
