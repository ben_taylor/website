#+STARTUP: showeverything

#+title:  Installing Jupyter Notebook Plus R Kernel on Linux Mint 21.1 XFCE
#+author: Benjamin Taylor
#+date:   2023-01-06

* Introduction

I was thinking about using Jupyter Notebook to administer elements one of my courses at UCC. With help from [[http://publish.ucc.ie/researchprofiles/D019/aamann][Andreas Amann]], we looked at the local install on lab computers here at [[https://www.ucc.ie/][UCC]], which are administered through the [[https://www.anaconda.com/][Anaconda]] platform. However, I wanted to get a local version running on my Linux Mint box, so modified [[https://developers.refinitiv.com/en/article-catalog/article/setup-jupyter-notebook-r][this]] post by Jirapongse Phuriphanvichai in order to do so.

* First the Python bits

Make sure you are using the latest version of pip and install jupyter

#+begin_src sh
python3 -m pip install --upgrade pip
python3 -m pip install jupyter
#+end_src

* Then Installing the Required Elements of Jupyter

Installing the following is necessary to get jupyter notebook running from the terminal and also to link it to R.

#+begin_src sh
sudo apt-get install jupyter-core
sudo apt-get install jupyter-notebook
sudo apt-get install jupyter-client
#+end_src

* Make the Kernel Visible to R 

Then it is necessary to install the IRkernel package in R. In the following, I found that it was necessary to run R as root:

#+begin_src sh
sudo R
#+end_src

Then in R, first install the package, then make the kernel visible to jupyter notebook

#+begin_src R
install.packages("IRkernel")
IRkernel::installspec(user = FALSE)
#+end_src

* Lastly ...

Now you can run jupyter notebook and select R as a kernel from the "New" drop down menu.

#+begin_src sh
jupyter notebook
#+end_src

If you have used the latest version of R to run this install, then you should also now be running this version within the notebook. You can of course check this by issuing 

#+begin_src R
version
#+end_src

at a prompt and running it.
