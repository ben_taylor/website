# update indices
sudo apt update -qq
# install two helper packages we need
sudo apt -y install --no-install-recommends software-properties-common dirmngr
# add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
# Fingerprint: E298A3A825C0D65DFD57CBB651716619E084DAB9
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"

sudo apt-get -y install r-base

sudo apt-get -y install libharfbuzz-dev
sudo apt-get -y install libfribidi-dev


sudo apt-get -y install cmake
sudo apt-get -y install libudunits2-dev
sudo apt-get -y install libfontconfig1-dev
sudo apt-get -y install jdk # THIS LINE NEEDS EDITING AS THE JAVA INSTALL IS A BIT MORE COMPLICATED ...
sudo apt-get -y install libproj
sudo apt-get -y install netcdf-bin
sudo apt-get -y install gdal-bin
sudo apt-get -y install libhdf5-dev
sudo apt-get -y install hdfview
sudo apt-get -y install libfreetype6-dev
sudo apt-get -y install libmagick-dev
sudo apt-get -y install perlmagick
sudo apt-get -y install libgdal-dev
sudo apt-get -y install libgeos
sudo apt-get -y install libudunits
sudo apt-get -y install bwidget

sudo apt-get -y install tex fonts
sudo apt-get -y install texlive-bibtex-extra
sudo apt-get -y install texlive-science
sudo apt-get -y install texinfo
sudo apt-get -y install latexmk
sudo apt-get -y install beamer

sudo apt-get -y install tcltk
sudo apt-get -y install libx11
sudo apt-get -y install libglu
sudo apt-get -y install rgl rpackage
sudo apt-get -y install fail2ban
sudo apt-get -y install ssh
sudo apt-get -y install libssl-dev
sudo apt-get -y install gfortran

# rstudio

#### ~/Dropbox/ncdf_1.8.6.tar.gz # no longer necessary.
#~/Dropbox/install_R_packages.R
####atom editor plus language-r, language-latex, atom-latex, latex, latex-autocomplete, minimap, multi-cursor, open-recent, advanced-open-file
#atom editor plus run the following
#apm install --packages-file atom-package-list.txt
#x2go client (details below)

#sudo apt-add-repository ppa:x2go/stable
#sudo apt-get update
#sudo apt-get install x2goserver x2goserver-xsession
#sudo apt-get install x2goclient

#sudo add-apt-repository -y ppa:opencpu/imagemagick
#sudo apt-get update
#sudo apt-get install -y libmagick++-dev


#QGIS
#chrome
#rstudio
#r-base
#dropbox
#skype
#cheese
#shutter
#evince
#### kile
#google talk plugin
#abcde (cd ripping)
