#+STARTUP: showeverything

#+title: Iománaíocht / Hurling -- 2023
#+subtitle: Division 7
#+author: Benjamin Taylor, Kevin Hayes and OTHERS
#+date: Thu Aug 10 16:25:04 2023 

* Overall Team Strength


#+CAPTION: Team Strength
#+ATTR_HTML: :width 500px
[[./fig/strength20237-1.png]]


* Goal and Point Scoring Strength


#+CAPTION: Goal Scoring Strength
#+ATTR_HTML: :width 500px
[[./fig/strengthg20237-1.png]]
#+CAPTION: Point Scoring Strength
#+ATTR_HTML: :width 500px
[[./fig/strengthp20237-1.png]]


* Home Pitch Advantage


#+CAPTION: Home Pitch Advantage
#+ATTR_HTML: :width 500px
[[./fig/homeadv20237-1.png]]


* Model Diagnostic Plots


#+ATTR_HTML: :width 500px :center nil
#+CAPTION: Residuals (1)
[[./fig/residuals20237-1.png]]
#+ATTR_HTML: :width 500px :center nil
#+CAPTION: Residuals (2)
[[./fig/fitres20237-1.png]]
