#+STARTUP: showeverything

#+title: Iománaíocht / Hurling -- 2023
#+subtitle: Division 1
#+author: Benjamin Taylor, Kevin Hayes and OTHERS
#+date: Thu Aug 10 16:24:56 2023 

* Overall Team Strength


#+CAPTION: Team Strength
#+ATTR_HTML: :width 500px
[[./fig/strength20231-1.png]]


* Goal and Point Scoring Strength


#+CAPTION: Goal Scoring Strength
#+ATTR_HTML: :width 500px
[[./fig/strengthg20231-1.png]]
#+CAPTION: Point Scoring Strength
#+ATTR_HTML: :width 500px
[[./fig/strengthp20231-1.png]]


* Home Pitch Advantage


#+CAPTION: Home Pitch Advantage
#+ATTR_HTML: :width 500px
[[./fig/homeadv20231-1.png]]


* Model Diagnostic Plots


#+ATTR_HTML: :width 500px :center nil
#+CAPTION: Residuals (1)
[[./fig/residuals20231-1.png]]
#+ATTR_HTML: :width 500px :center nil
#+CAPTION: Residuals (2)
[[./fig/fitres20231-1.png]]
